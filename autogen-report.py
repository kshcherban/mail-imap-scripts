#!/usr/bin/python
import sys
from imaplib import IMAP4
if len(sys.argv) != 3:
    print 'User or password not provided. Usage:', sys.argv[0], \
            '<username> <password>'
    sys.exit()
user = sys.argv[1]
passwd = sys.argv[2]
m = IMAP4('mail.hostopia.com')
m.login(user, passwd)
print "Sent messages selected", m.select('Sent')
typ, data = m.select('Sent')
# search by subject
#typ, data = m.search(None, '(SUBJECT "'+ 'Re' + '") SINCE 28-Nov-2011')
typ, data = m.search(None, '(SUBJECT "'+ 'Service Alert' + '" SINCE 05-Dec-2011)')
#This will print subjects of searched messaged
array = []
for num in data[0].split():
  typ, data = m.fetch(num, '(RFC822.SIZE BODY[HEADER.FIELDS (SUBJECT DATE])')
  message = str(data[0][1].splitlines())
  print message.replace('[', '').replace("'","").replace(']','').replace(',','').replace('-0500','|').replace('Date: ','')
m.close()
m.logout()
