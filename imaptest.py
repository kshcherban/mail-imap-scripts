#!/usr/bin/env python

# Copyright 2012-07-17 Konstantin Shcherban <k.scherban@gmail.com>

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE See the
# GNU General Public License for more details.

# login to the imap server and list the number of emails and their size in all folders

import imaplib
import sys
import re
import getpass

num_args = len(sys.argv)
if num_args != 3:
    print ""
    print "Usage:", sys.argv[0], "<imap.example.com> <username@example.com>"
    print ""
    sys.exit()


class Color:

    def __init__(self):
        self.r = "\033[01;31m"
        self.g = "\033[01;32m"
        self.b = "\033[01;34m"
        self.reset = "\033[0m"

    def red(self, s):
        res = self.r + s + self.reset
        return res

    def green(self, s):
        res = self.g + s + self.reset
        return res

    def blue(self, s):
        res = self.b + s + self.reset
        return res


C = Color()

imap_server = sys.argv[1]
imap_user = sys.argv[2]
imap_passwd = getpass.getpass()
server = imaplib.IMAP4(imap_server)
try:
    resp = server.login(imap_user, imap_passwd)
except imaplib.IMAP4.error, err:
    print C.red("Error!"), err
    sys.exit(1)
print ''
print resp
result, foldlist = server.list(directory = '""', pattern = '*')  # Raw list
# Prased list of folders
pattern = re.compile(r'\((?P<flags>.*?)\) "(?P<delimiter>.*)" (?P<name>.*)')
folder_list = []
for f in foldlist:
    try:
        folder_list.append(pattern.match(f).groups()[-1].strip('"'))
    except:
        folder_list.append('INBOX')
if folder_list == []:
    print "No folders found"
    sys.exit(1)
# Printing header of table
print ""
print "%-30s%15s%20s\n" % ("Folder", "# Msg", "Size(KB)")
number_of_messages_all = 0
size_all = 0
for mailbox in folder_list:
    # Select the desired folder
    result, number_of_messages = server.select(mailbox, readonly = 1)
    number_of_messages_all += int(number_of_messages[0])
    size_folder = 0
    m = int(number_of_messages[0])
    # For folders where are messages we count their size
    if m > 0:
        message_set = "%d:%d" % (1, m)
        result, sizes_response = server.fetch(message_set, "(UID RFC822.SIZE)")
        size_folder = sum([int(i.split()[-1].replace(')', '')) for i in sizes_response])
    else:
        size_folder = 0
    print "%-30s%15d%20.3f" % (mailbox, int(number_of_messages[0]), size_folder / (2.**10))
    size_all += size_folder

print "\n%-30s%15i%20.3f MB\n" % ("Sum", number_of_messages_all, size_all / ((2.**10)**2))
server.close
server.logout
