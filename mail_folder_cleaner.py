#!/usr/bin/python
# kshcherban GPL v3

from imaplib import IMAP4
import sys, string, getopt

def usage():
  usage = """
  Mail folder cleaner 1.0
  
  usage: %s [arguments]
  
  arguments:
  -h --help                 Print Help (this message) and exit
  -e --email                Email account address, with '@'
  -p --password             Password for provided account
  -f --folder               Folder for provided account
  -s --server               IMAP mail server address [required only if it is custom]
  """ % (sys.argv[0])
  print usage
  sys.exit()

if len(sys.argv) < 4:
  usage()
  sys.exit()
  
def main(argv):
  try:
    opt, args = getopt.getopt(argv, "he:pf:s:", ["help", "email=", "password=", "folder=", "server="])
  except getopt.GetoptError, err:
    print str(err)
    usage()
    sys.exit(2)
  output = None
  verbose = False
  for o, a in opt:
    if o in ("-h", "--help"):
      usage()
      sys.exit()
    elif o in ("-e", "--email"):
      email = a
    elif o in ("-p", "--password"):
      password = a
    elif o in ("-f", "--folder"):
      folder = a
    elif o in ("-s", "--server"):
      server = a
    # additional functions may be here later
    else:
      assert False, "unhandled option"
  try: server
  except: 
    try: server = 'mail.'+email.split('@')[1]
    except IndexError:
      print "Please specify email username with @."
      sys.exit()
  return email, password, server

if __name__ == "__main__":
  main(sys.argv[1:])
  m = IMAP4(server)
  try: m.login(email, password)
  except:
    print "Incorrect username/password specified!"
    sys.exit(1)
  # shows how many messages in selected folder
  typ, data = m.select(folder)
  typ, data = m.search(None, 'ALL')
  me = [int(x) for x in data[0].split()]
  me.sort()
  try: 
    message_set = "%d:%d" % (me[0], me[-1])
    num = me[-1]
  except IndexError:
    num = 0
  # num = int(m.select(mailfold)[1][0])
  if num == 0:
    print "Nothing to remove, 0 msgs in folder", folder
    print "Exiting..."
    sys.exit(1)
  print "Will be removed", num, "messages from folder", folder
  print "Are you sure?"
  decision = raw_input("Enter y or n: ")
  if decision == 'y' or decision == 'Y':
    pass
  else:
    print "Exiting..."
    sys.exit(1)
  typ, data = m.select(folder)
  print 'Removing messages...'
  #function to replace previous line with new 
  def restart_line():
    sys.stdout.write('\r')
    sys.stdout.flush()
  m.store(message_set, "+FLAGS", '\\Deleted')
  #deprecated stupid iteration
  """
  for i in range(1,num+1):
    m.store(i, "+FLAGS", '\\Deleted')
    sys.stdout.write('Removing message: %s' % i)
    sys.stdout.flush()
    restart_line()
  """
  print "Expunging..."
  m.expunge()
  m.close()
  m.logout()
  print "Done! :)"
  
